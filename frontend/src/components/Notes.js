import React, { useState, useEffect, useRef } from "react";

const API = process.env.REACT_APP_API;

export const Notes = () => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [time, setTime] = useState("");

  const [editing, setEditing] = useState(false);
  const [id, setId] = useState("");

  const nameInput = useRef(null);

  let [notes, setNotes] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!editing) {
      const res = await fetch(`${API}/notes`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          content,
          time,
        }),
      });
      await res.json();
    } else {
      const res = await fetch(`${API}/notes/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          content,
          time,
        }),
      });
      const data = await res.json();
      console.log(data);
      setEditing(false);
      setId("");
    }
    await getNotes();

    setTitle("");
    setContent("");
    setTime("");
    nameInput.current.focus();
  };

  const getNotes = async () => {
    const res = await fetch(`${API}/notes`);
    const data = await res.json();
    setNotes(data);
  };

  const cancelUpdate = () => {
    setTitle("");
    setContent("");
    setTime("");
    setEditing(false);
  };

  const deleteNote = async (id) => {
    const noteResponse = window.confirm("Are you sure you want to delete it?");
    if (noteResponse) {
      const res = await fetch(`${API}/notes/${id}`, {
        method: "DELETE",
      });
      const data = await res.json();
      console.log(data);
      await getNotes();
    }
  };

  const editNote = async (id) => {
    const res = await fetch(`${API}/notes/${id}`);
    const data = await res.json();

    setEditing(true);
    setId(id);

    // Reset
    setTitle(data.title);
    setContent(data.content);
    setTime(data.time);
    nameInput.current.focus();
  };

  useEffect(() => {
    getNotes();
  }, []);

  return (
    <div className="row">
      <div className="col-md-4">
        <form onSubmit={handleSubmit} className="card card-body">
          <div className="form-group">
            <input
              type="text"
              onChange={(e) => setTitle(e.target.value)}
              value={title}
              className="form-control"
              placeholder="Title"
              ref={nameInput}
              autoFocus
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              onChange={(e) => setContent(e.target.value)}
              value={content}
              className="form-control"
              placeholder="Content"
            />
          </div>
          <div className="form-group">
            <input
              type="date"
              onChange={(e) => setTime(e.target.value)}
              value={time}
              className="form-control"
              placeholder="Date"
            />
          </div>
          <button className="btn btn-primary btn-block">
            {editing ? "Update" : "Create"}
          </button>
        </form>
        {editing ? <button 
        className="btn btn-danger btn-block"
        onClick={() => cancelUpdate()}>
              Cancel
          </button> : ""}
      </div>
      <div className="col-md-8">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Title</th>
              <th>Content</th>
              <th>Date</th>
              <th>Operations</th>
            </tr>
          </thead>
          <tbody>
            {notes.map((note) => (
              <tr key={note._id}>
                <td>{note.title}</td>
                <td>{note.content}</td>
                <td>{note.time}</td>
                <td>
                  <button
                    className="btn btn-outline-warning btn-block"
                    onClick={(e) => editNote(note._id)}
                  >
                    Edit
                  </button>
                  <button
                    className="btn btn-outline-danger btn-block"
                    onClick={(e) => deleteNote(note._id)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};