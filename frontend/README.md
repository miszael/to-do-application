## Ejecutar Frontend

Ejecutar los suguientes comandos dentro del directorio del proyecto --> \to-do-app\frontend\

## Project setup
```
npm install
```

## Project start
```
npm run start
```
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

