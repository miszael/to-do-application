from flask import Flask, jsonify, request
from flask_pymongo import PyMongo
from flask_cors import CORS

from bson import ObjectId

# Instantiation
app = Flask(__name__)
app.config['MONGO_URI'] = 'mongodb://localhost:27017/ToDo-database'
mongo = PyMongo(app)

# Settings
CORS(app)

# Database
db = mongo.db.pyNotes

# Routes
@app.route('/notes', methods=['POST'])
def createNote():
  print(request.json)
  id = db.insert({
    'title': request.json['title'],
    'content': request.json['content'],
    'time': request.json['time']
  })
  return jsonify(str(ObjectId(id)))


@app.route('/notes', methods=['GET'])
def getNotes():
    notes = []
    for doc in db.find():
        notes.append({
            '_id': str(ObjectId(doc['_id'])),
            'title': doc['title'],
            'content': doc['content'],
            'time': doc['time']
        })
    return jsonify(notes)

@app.route('/notes/<id>', methods=['GET'])
def getNote(id):
  note = db.find_one({'_id': ObjectId(id)})
  print(note)
  return jsonify({
      '_id': str(ObjectId(note['_id'])),
      'title': note['title'],
      'content': note['content'],
      'time': note['time']
  })


@app.route('/notes/<id>', methods=['DELETE'])
def deleteNote(id):
  db.delete_one({'_id': ObjectId(id)})

@app.route('/notes/<id>', methods=['PUT'])
def updateNote(id):
  print(request.json)
  db.update_one({'_id': ObjectId(id)}, {"$set": {
    'title': request.json['title'],
    'content': request.json['content'],
    'time': request.json['time']
  }})
  return jsonify({'message': 'Note Updated'})
if __name__ == "__main__":
    app.run(debug=True)
