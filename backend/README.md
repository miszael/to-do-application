## Activar API
Ejecutar los suguientes comandos dentro del directorio del proyecto --> \to-do-app\backend\
## Instalar virtualenv

```
pip install virtualenv
```
## Crear entorno virtual

```
virtualenv venv
```
## Ejecutar entorno virtual (si no se ejecutó automaticamente)

```
.\venv\Scripts\activate
```
<img src="https://gitlab.com/miszael/to-do-application/-/blob/main/2021-08-06%20182831.png" alt="Ejemplo">

## Project start

```
python .\src\app.py
```